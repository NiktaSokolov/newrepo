
public class Rectangle extends Figure {

    private double a;
    private double b;
    private double S;

    public Rectangle(int x, int y) {
        super(x, y);

    }
    public void getQuadrant(int x, int y) {
        super.getQuadrant(x, y);
    }


    @Override
    public String figureName() {
        return "Прямоугольник ----- ";
    }

    @Override
    public void square() {
        int first = 1;
        int last = 20;
        a = first + (int) (Math.random()* last);
        b = first + (int) (Math.random()* last);
            S = a * b;
            System.out.println("Площадь прямоугольника: " + S);


    }


    public double getA() {
        return a;
    }

    public void setA() {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB() {
        this.b = b;
    }

    public double getS() {
        return S;
    }

    public void setS() {
        this.S = S;
    }
}
