package ru.unn;

import java.util.Arrays;

public class Main2 {

    public static void main(String[] args) {
        double[] arr = {1, 6, 9, 4, 10, 8, 5, 2, 7, 3};
        for (int i = 0; i < arr.length; i++) {
            arr[i] += arr[i] * 0.1;
        }
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j+1]) {
                    double tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        String arrStr = Arrays.toString(arr);
        System.out.println(arrStr);
    }
}
